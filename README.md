# 电梯井道信号模拟器

#### 介绍
电梯井道模拟器，基于STM32F407VET6输出控制继电器，模拟电梯运行所需要的信号。

#### 软件架构
使用c与库函数编写


#### 安装教程

1.  xxxx


#### 使用说明

通过STM32模拟电梯运行所需的井道信号，用于维修测试电梯控制系统
代码缺点较多，全局变量过多，串口修改变量值繁琐。因技术有限。只能完成功能，代码繁琐，不知怎么优化，希望有大神可以修改优化，不用全局变量，使用局部变量，掉电保存参数。使用外部存储芯片w25q16，

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
